FROM node:lts-alpine as builder

ARG VERSION_ID

ENV VERSION_ID ${VERSION_ID:-none}
ENV SENTRY_RELEASE $VERSION_ID

RUN apk add --no-cache inotify-tools

RUN mkdir -p /srv

WORKDIR /srv

COPY package.json /srv
COPY package-lock.json /srv

RUN NODE_ENV=development npm install

COPY . /srv

FROM node:lts-alpine as maker

ARG VERSION_ID

ENV VERSION_ID ${VERSION_ID:-none}
ENV SENTRY_RELEASE $VERSION_ID

RUN mkdir -p /srv

COPY --from=builder /srv /srv

WORKDIR /srv

RUN npm run build
RUN npm prune

FROM keymetrics/pm2:latest-alpine

ARG VERSION_ID

ENV VERSION_ID ${VERSION_ID:-none}
ENV SENTRY_RELEASE $VERSION_ID

COPY --from=maker /srv /srv

WORKDIR /srv

EXPOSE 3000

CMD pm2-runtime start ecosystem.config.js
