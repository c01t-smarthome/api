import {
  Route,
  httpGet
} from '@viatsyshyn/ts-rest';
import {OpenApiPath, OpenApiOperationGet} from '@viatsyshyn/ts-openapi';
import {VersionResponseModel} from '../../models';

OpenApiPath({
  path: '/api/versions',
  tag: 'Version'
})

@httpGet('/api/versions')
export class Versions extends Route {

  private versions: any[] = [
    {
      id: '1',
      name: 'Version 1',
      description: 'Description Version 1',
      version: '1.0.0'
    }
  ];

  @OpenApiOperationGet({
    path: '/api/versions',
    description: 'Get versions objects list',
    summary: 'Get versions list',
    responses: {
      200: {content: [VersionResponseModel]}
    }
  })
  public async handle(): Promise<void> {
    this.json(this.versions);
  }
}
