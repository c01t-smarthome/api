import {inject} from 'inversify';
import {
  Route,
  httpPost,
  requestBody
} from '@viatsyshyn/ts-rest';
import {OpenApiOperationPost} from '@viatsyshyn/ts-openapi';

import {IUserService} from '../../services';
import {IJWTCONFIG, IJwtConfig} from '../../authentication';
import * as jwt from 'jsonwebtoken';
import {AuthenticateModel} from '../../models/input';
import {AuthenticationResponseModel, ErrorResponseModel} from '../../models/output';

@httpPost('/api/auth/login')
export class LoginRoute extends Route {

  @inject(Symbol.for('IUserService'))
  private userService: IUserService;

  @inject(IJWTCONFIG)
  private jwtConfig: IJwtConfig;

  @OpenApiOperationPost({
    path: '/api/auth/login',
    description: 'Login',
    request: {
      body: {
        description: 'Authenticate user',
        required: true,
        schema: AuthenticateModel
      }
    },
    responses: {
      200: {content: AuthenticationResponseModel},
      400: {content: ErrorResponseModel},
      404: {content: ErrorResponseModel},
    }
  })
  public async handle(
    @requestBody() { username, password }: AuthenticateModel
  ): Promise<void> {

    let user = await this.userService.authenticate(username, password);
    if (!user) {
      return this.json<AuthenticationResponseModel>({ auth: false, token: null });
    }

    const token = jwt.sign({...user}, this.jwtConfig.secret);
    return this.json<AuthenticationResponseModel>({ auth: true, token });
  }
}
