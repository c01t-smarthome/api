import {OpenApiPath} from '@viatsyshyn/ts-openapi';

export * from './login.route';
export * from './logout.route';

OpenApiPath({
  path: '/api/auth',
  tag: 'Authentication',
})
