import { httpPost, Route } from '@viatsyshyn/ts-rest';
import { OpenApiOperationPost } from '@viatsyshyn/ts-openapi';

@httpPost('/api/auth/logout')
export class LogoutRoute extends Route {

  @OpenApiOperationPost({
    path: '/api/auth/logout',
    description: 'Log out',
    summary: 'log out',
    responses: {
      204: {}
    }
  })
  public async handle(): Promise<void> {
    return this.empty(204);
  }
}
