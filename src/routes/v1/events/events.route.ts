import {httpGet, Route} from '@viatsyshyn/ts-rest';
import {AuthMiddleware} from '../../../middleware';
import {inject} from 'inversify';
import {ICache, ICACHE} from '../../../services/redis';
import {IEntityService, Symbols} from '@viatsyshyn/ts-entity';
import {AttributeModel, DeviceModel, EventResponse, ZoneModel} from '../../../models';
import {OpenApiOperationGet} from '@viatsyshyn/ts-openapi';
import {ISseResponse, SSEMiddleware} from '../../../utils/sse';
import {ILogger, ILOGGERFACTORY, ILoggerFactory} from '../../../services/logger';
import {IPubSub, IPUBSUB} from '../../../services/mqtt';

@httpGet('/api/v1/events', AuthMiddleware, SSEMiddleware)
export class EventsRoute extends Route {

  private logger: ILogger;

  @inject(ICACHE)
  private cache: ICache;

  @inject(IPUBSUB)
  private pubsub: IPubSub;

  @inject(Symbols.serviceFor(ZoneModel))
  private zoneService: IEntityService<ZoneModel>;

  @inject(Symbols.serviceFor(DeviceModel))
  private deviceService: IEntityService<DeviceModel>;

  @inject(Symbols.serviceFor(AttributeModel))
  private attributeService: IEntityService<AttributeModel>;

  protected response: ISseResponse;

  constructor(
    @inject(ILOGGERFACTORY) loggerFactory: ILoggerFactory,
  ) {
    super();
    this.logger = loggerFactory('events-route');
  }

  @OpenApiOperationGet({
    path: '/api/v1/events',
    description: `Get events`,
    summary: `Get events`,
    responses: {
      200: {
        content: {
          'text/event-stream': EventResponse
        }
      }
    }
  })
  public async handle(): Promise<void> {
    this.logger.log('here');

    try {
      await new Promise((resolve, reject) => {
        const handler = async (value: any, topic: string) => {
          try {
            const [tag, , key] = topic.split('/');

            const device = await this.deviceService.getOne({tag});
            if (!device) {
              return ;
            }

            const attribute = await this.attributeService.getOne({device: device.id, kind: 'out', key});
            if (!attribute) {
              return ;
            }

            const model: EventResponse = {
              device: device.uid,
              key: key,
              lastUpdated: await this.cache.get(tag, 'last-updated', null),
              value
            };

            this.response.pushEvent('reported', model, Date.now().toString(36));
          } catch (e) {
            finish();
            reject(e);
          }
        };

        this.pubsub.sub('+/reported/+', handler);

        const finish = () => {
          this.pubsub.unsub('+/reported/+', handler);
          this.response.end();
          resolve();
        };

        this.response.once('close', finish);
      });

      this.logger.log('closed');
    } catch (e) {
      this.response.pushEvent('error', {
        code: 500,
        message: e.message || e
      });
      this.response.end();
    }
  }
}
