import {OpenApiPath} from '@viatsyshyn/ts-openapi';

export * from './events.route';

OpenApiPath({
  path: '/api/v1/events',
  tag: 'Events',
  security: [{ BearerAuth: [] }]
});
