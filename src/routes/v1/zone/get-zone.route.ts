import { Route, httpGet, requestParam } from '@viatsyshyn/ts-rest';
import {OpenApiOperationGet} from '@viatsyshyn/ts-openapi';
import { AuthMiddleware } from '../../../middleware';
import { inject } from 'inversify';
import { WhereBuilder, OrderBuilder } from '@viatsyshyn/ts-orm';
import {IEntityService, Symbols} from '@viatsyshyn/ts-entity';
import { QueryParserMiddlewareFor } from '@viatsyshyn/ts-rest-query-to-orm';
import {COUNT_ONLY, LIMIT, OFFSET, SORT} from '../../../models';
import {ZoneModel} from '../../../models';
import {ZoneResponseModel} from '../../../models';
import {GetZoneModel} from '../../../models';

@httpGet('/api/v1/zone', AuthMiddleware, QueryParserMiddlewareFor(GetZoneModel))
export class GetZoneRoute extends Route {

  @inject(Symbols.serviceFor(ZoneModel))
  private zoneService: IEntityService<ZoneModel>;

  @OpenApiOperationGet({
    path: '/api/v1/zone',
    description: `Get zones`,
    summary: `Get zones`,
    request: {
      query: {
        sort: SORT,
        offset: OFFSET,
        limit: LIMIT,
        count: COUNT_ONLY,
      }
    },
    responses: {
      200: {content: [ZoneResponseModel]}
    }
  })
  public async handle(
    @requestParam('query') query: WhereBuilder<ZoneModel>,
    @requestParam('orderBy') orderBy: OrderBuilder<ZoneModel>,
    @requestParam('limit') limit: number,
    @requestParam('offset') offset: number,
    @requestParam('countOnly') countOnly: boolean
  ): Promise<void> {

    let baseQuery = this.zoneService.select();
    if (query) {
      baseQuery = baseQuery.where(query);
    }

    if (countOnly) {
      this.response.status(200).json(await baseQuery.count());
      return ;
    }

    const zones = await baseQuery
      .limit(limit)
      .offset(offset)
      .orderBy(orderBy)
      .fetch<ZoneModel>();

    return this.json(zones);
  }
}
