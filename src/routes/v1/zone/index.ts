import {OpenApiPath} from '@viatsyshyn/ts-openapi';

export * from './get-zone.route';
export * from './post-zone.route';
export * from './put-zone.route';
export * from './delete-zone.route';

OpenApiPath({
  path: '/api/v1/zone',
  tag: 'Zone',
  security: [{ BearerAuth: [] }]
});
