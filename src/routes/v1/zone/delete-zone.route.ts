import {httpDelete, requestParam, Route} from '@viatsyshyn/ts-rest';
import {OpenApiOperationDelete} from '@viatsyshyn/ts-openapi';
import {AuthMiddleware} from '../../../middleware';
import {ErrorResponseModel, UID, ZoneModel} from '../../../models';
import {inject} from 'inversify';
import {IEntityService, IUnitOfWork, Symbols} from '@viatsyshyn/ts-entity';
import {NotFoundError} from '@viatsyshyn/ts-orm';

@httpDelete('/api/v1/zone/:uid', AuthMiddleware)
export class DeleteZoneRoute extends Route {

  @inject(Symbols.UnitOfWork)
  private unitOfWork: IUnitOfWork;

  @inject(Symbols.serviceFor(ZoneModel))
  private zoneService: IEntityService<ZoneModel>;

  // @inject(Validator)
  // @named('UUID')
  // private validator: IValidator<string>;

  @OpenApiOperationDelete({
    path: '/api/v1/zone/{uid}',
    description: `Delete Zone`,
    summary: `Delete Zone`,
    request: {
      path: {
        uid: UID
      }
    },
    responses: {
      204: {},
      400: {content: ErrorResponseModel},
      404: {content: ErrorResponseModel},
    }
  })
  public async handle(
    @requestParam('uid') uid: string
  ): Promise<void> {

    // const valid = this.validator.validate(uid, {required: true, notNullable: true});
    // if (valid !== true) {
    //   throw new BadRequestError('Invalid subscription Id');
    // }

    const zone = await this.zoneService.getOne({uid});
    if (!zone) {
      throw new NotFoundError(`Zone ${uid} not found`);
    }

    await this.unitOfWork.auto(() => this.zoneService.delete({id: zone.id}));

    return this.empty(204);
  }
}
