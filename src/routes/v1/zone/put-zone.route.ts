import {
  BadRequestError,
  httpPut,
  requestBody,
  requestParam,
  Route,
} from '@viatsyshyn/ts-rest';
import {OpenApiOperationPut} from '@viatsyshyn/ts-openapi';
import {ILogger, ILoggerFactory, ILOGGERFACTORY} from '../../../services/logger';
import {inject, named} from 'inversify';
import {IEntityService, IUnitOfWork, Symbols} from '@viatsyshyn/ts-entity';
import {ErrorResponseModel, UID, ZoneModel} from '../../../models';
import {PutZoneModel} from '../../../models/input';
import {IValidator, Validator} from '@viatsyshyn/ts-validate';
import {ZoneResponseModel} from '../../../models/output';
import {NotFoundError} from '@viatsyshyn/ts-orm';
import {AuthMiddleware} from '../../../middleware';

@httpPut('/api/v1/zone/:uid', AuthMiddleware)
export class PutZoneRoute extends Route {

  private readonly logger: ILogger;

  @inject(Symbols.UnitOfWork)
  private unitOfWork!: IUnitOfWork;

  @inject(Validator)
  @named(PutZoneModel.name)
  private validator: IValidator<PutZoneModel>;

  @inject(Symbols.serviceFor(ZoneModel))
  private zoneService!: IEntityService<ZoneModel>;

  public constructor(
    @inject(ILOGGERFACTORY) loggerFactory: ILoggerFactory,
  ) {
    super();
    this.logger = loggerFactory('put-zone-handler');
  }

  @OpenApiOperationPut({
    path: '/api/v1/zone/{uid}',
    description: `Update Zone`,
    summary: `Update Zone`,
    request: {
      path: {
        uid: UID
      },
      body: {
        description: `Zone data`,
        required: true,
        schema: PutZoneModel,
      }
    },
    responses: {
      200: {content: ZoneResponseModel},
      400: {content: ErrorResponseModel},
      404: {content: ErrorResponseModel},
    }
  })
  public async handle(
    @requestParam('uid') uid: string,
    @requestBody() data: PutZoneModel
  ): Promise<void> {
    if (!data) {
      throw new BadRequestError('Put data is empty');
    }

    const valid = this.validator.validate(data, {requireRequired: true});
    if (valid !== true) {
      throw new BadRequestError(valid.join(', '));
    }

    let zone = await this.zoneService.getOne({uid});
    if (!zone) {
      throw new NotFoundError(`Zone ${uid} not found`);
    }

    const model: Partial<ZoneModel> = {
      id: zone.id,
      ...data,
    };
    zone = await this.unitOfWork.auto(async () => this.zoneService.updateOne(model));

    return this.json(zone);
  }

}
