import {
  BadRequestError,
  httpPost,
  requestBody,
  Route
} from '@viatsyshyn/ts-rest';
import {OpenApiOperationPost} from '@viatsyshyn/ts-openapi';
import {ILogger, ILoggerFactory, ILOGGERFACTORY} from '../../../services/logger';
import {inject, named} from 'inversify';
import {IEntityService, IUnitOfWork, Symbols} from '@viatsyshyn/ts-entity';
import {ErrorResponseModel, ZoneModel} from '../../../models';
import {PostZoneModel} from '../../../models/input';
import {IValidator, Validator} from '@viatsyshyn/ts-validate';
import {ZoneResponseModel} from '../../../models/output';
import {AuthMiddleware} from '../../../middleware';

@httpPost('/api/v1/zone', AuthMiddleware)
export class PostZoneRoute extends Route {

  private readonly logger: ILogger;

  @inject(Symbols.UnitOfWork)
  private unitOfWork!: IUnitOfWork;

  @inject(Validator)
  @named(PostZoneModel.name)
  private validator: IValidator<PostZoneModel>;

  @inject(Symbols.serviceFor(ZoneModel))
  private zoneService!: IEntityService<ZoneModel>;

  public constructor(
    @inject(ILOGGERFACTORY) loggerFactory: ILoggerFactory,
  ) {
    super();
    this.logger = loggerFactory('post-zone-handler');
  }

  @OpenApiOperationPost({
    path: '/api/v1/zone',
    description: `Create Zone`,
    summary: `Create Zone`,
    request: {
      body: {
        description: `Zone data`,
        required: true,
        schema: PostZoneModel,
      }
    },
    responses: {
      200: {content: ZoneResponseModel},
      400: {content: ErrorResponseModel},
    }
  })
  public async handle(
    @requestBody() data: PostZoneModel
  ): Promise<void> {
    if (!data) {
      throw new BadRequestError('Post data is empty');
    }

    const valid = this.validator.validate(data, {requireRequired: true});
    if (valid !== true) {
      throw new BadRequestError(valid.join(', '));
    }

    const model: Partial<ZoneModel> = {
      ...data,
    };

    const zone = await this.unitOfWork.auto(async () => this.zoneService.addOne(model));

    return this.json(zone);
  }

}
