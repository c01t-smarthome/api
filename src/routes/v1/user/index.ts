import {OpenApiPath} from '@viatsyshyn/ts-openapi';

export * from './put-user.route';

OpenApiPath({
  path: '/api/v1/user',
  tag: 'User',
  security: [{ BearerAuth: [] }]
});
