import {
  BadRequestError,
  httpPut,
  requestBody,
  requestParam,
  Route,
} from '@viatsyshyn/ts-rest';
import {ILogger, ILoggerFactory, ILOGGERFACTORY} from '../../../services/logger';
import {inject, named} from 'inversify';
import {ForbiddenError} from '@viatsyshyn/ts-rest';
import {IUnitOfWork, Symbols} from '@viatsyshyn/ts-entity';
import {ErrorResponseModel, UID, UserModel} from '../../../models';
import {PutZoneModel} from '../../../models';
import {IValidator, Validator} from '@viatsyshyn/ts-validate';
import {NotFoundError} from '@viatsyshyn/ts-orm';
import {AuthMiddleware} from '../../../middleware';
import {PutUserModel} from '../../../models/input';
import {UserResponseModel} from '../../../models';
import {IUserService} from '../../../services/user';
import {UserType} from '../../../models';
import {OpenApiOperationPut} from '@viatsyshyn/ts-openapi';

@httpPut('/api/v1/user/:uid', AuthMiddleware)
export class PutUserRoute extends Route {

  private readonly logger: ILogger;

  @inject(Symbol.for('CurrentUser'))
  private me!: UserModel;

  @inject(Symbols.UnitOfWork)
  private unitOfWork!: IUnitOfWork;

  @inject(Validator)
  @named(PutZoneModel.name)
  private validator: IValidator<PutUserModel>;

  @inject(Symbol.for('IUserService'))
  private userService!: IUserService;

  public constructor(
    @inject(ILOGGERFACTORY) loggerFactory: ILoggerFactory,
  ) {
    super();
    this.logger = loggerFactory('put-user-handler');
  }

  @OpenApiOperationPut({
    path: '/api/v1/user/{uid}',
    description: `Update User`,
    summary: `Update User`,
    request: {
      path: {
        uid: UID,
      },
      body: {
        description: `User data`,
        required: true,
        schema: PutUserModel,
      }
    },
    responses: {
      200: {content: UserResponseModel},
      400: {content: ErrorResponseModel},
      404: {content: ErrorResponseModel},
    }
  })
  public async handle(
    @requestParam('uid') uid: string,
    @requestBody() data: PutUserModel
  ): Promise<void> {
    if (!data) {
      throw new BadRequestError('Put data is empty');
    }

    const valid = this.validator.validate(data, {requireRequired: true});
    if (valid !== true) {
      throw new BadRequestError(valid.join(', '));
    }

    if (uid === 'me') {
      uid = this.me.uid;

      const auth = await this.userService.authenticate(this.me.userName, data.password!);
      if (!auth) {
        throw new BadRequestError(`Confirmation and current password doesn't match`);
      }
    }

    let user = await this.userService.getOne({uid});
    if (!user) {
      throw new NotFoundError(`Zone ${uid} not found`);
    }

    if (this.me.type !== UserType.Admin && this.me.id !== user.id) {
      throw new ForbiddenError(`Current user is not allowed to perform this operation`);
    }

    const model: Partial<UserModel> = {
      id: user.id,
      ...data,
    };
    user = await this.unitOfWork.auto(async () => this.userService.updateOne(model));

    return this.json(user);
  }

}
