import {
  BadRequestError,
  httpPut,
  requestBody,
  requestParam,
  Route,
} from '@viatsyshyn/ts-rest';
import {ILogger, ILoggerFactory, ILOGGERFACTORY} from '../../../services/logger';
import {inject, named} from 'inversify';
import {IEntityService, IUnitOfWork, Symbols} from '@viatsyshyn/ts-entity';
import {DeviceResponseModel, ErrorResponseModel, UID, ZoneModel} from '../../../models';
import {PutDeviceModel} from '../../../models';
import {IValidator, Validator} from '@viatsyshyn/ts-validate';
import {NotFoundError} from '@viatsyshyn/ts-orm';
import {AuthMiddleware} from '../../../middleware';
import {DeviceModel} from '../../../models';
import {OpenApiOperationPut} from '@viatsyshyn/ts-openapi';
import {ICache, ICACHE} from '../../../services/redis';

@httpPut('/api/v1/device/:uid', AuthMiddleware)
export class PutDeviceRoute extends Route {

  private readonly logger: ILogger;

  @inject(ICACHE)
  private cache!: ICache;

  @inject(Symbols.UnitOfWork)
  private unitOfWork!: IUnitOfWork;

  @inject(Validator)
  @named(PutDeviceModel.name)
  private validator: IValidator<PutDeviceModel>;

  @inject(Symbols.serviceFor(ZoneModel))
  private zoneService!: IEntityService<ZoneModel>;

  @inject(Symbols.serviceFor(DeviceModel))
  private deviceService!: IEntityService<DeviceModel>;

  public constructor(
    @inject(ILOGGERFACTORY) loggerFactory: ILoggerFactory,
  ) {
    super();
    this.logger = loggerFactory('put-device-handler');
  }

  @OpenApiOperationPut({
    path: '/api/v1/device/{uid}',
    description: `Update Device`,
    summary: `Update Device`,
    request: {
      path: {
        uid: UID,
      },
      body: {
        description: `Device data`,
        required: true,
        schema: PutDeviceModel,
      }
    },
    responses: {
      200: {content: DeviceResponseModel},
      400: {content: ErrorResponseModel},
      404: {content: ErrorResponseModel},
    }
  })
  public async handle(
    @requestParam('uid') uid: string,
    @requestBody() data: PutDeviceModel,
  ): Promise<void> {
    if (!data) {
      throw new BadRequestError('Put data is empty');
    }

    const valid = this.validator.validate(data, {requireRequired: true});
    if (valid !== true) {
      throw new BadRequestError(valid.join(', '));
    }

    let device = await this.deviceService.getOne({uid});
    if (!device) {
      throw new NotFoundError(`Zone ${uid} not found`);
    }

    const zone = await this.zoneService.getOne(data.zone ? {uid: data.zone} : {id: device.zone});

    const model: Partial<DeviceModel> = {
      id: device.id,
      ...data,
      zone: zone ? zone.id : null,
    };
    device = await this.unitOfWork.auto(async () => this.deviceService.updateOne(model));

    const {tag, icons, widgets, id, ...rest} = device;
    return this.json({
      ...rest,
      tag,
      assets: icons,
      icon: widgets.icon,
      widgets: widgets.device,
      zone: zone ? zone.uid : null,
      attributes: await this.cache.getAll(tag),
    });
  }
}
