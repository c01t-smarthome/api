import {
  BadRequestError,
  httpPost,
  requestBody,
  Route,
} from '@viatsyshyn/ts-rest';
import {ILogger, ILoggerFactory, ILOGGERFACTORY} from '../../../services/logger';
import {inject, named} from 'inversify';
import {IEntityService, IUnitOfWork, Symbols} from '@viatsyshyn/ts-entity';
import {ErrorResponseModel} from '../../../models';
import {PostDeviceModel} from '../../../models';
import {IValidator, Validator} from '@viatsyshyn/ts-validate';
import {AuthMiddleware} from '../../../middleware';
import {DeviceModel} from '../../../models';
import {OpenApiOperationPost} from '@viatsyshyn/ts-openapi';
import {IPUBSUB, IPubSub} from '../../../services/mqtt';

@httpPost('/api/v1/device', AuthMiddleware)
export class PostDeviceRoute extends Route {

  private readonly logger: ILogger;

  @inject(IPUBSUB)
  private pubsub: IPubSub;

  @inject(Symbols.UnitOfWork)
  private unitOfWork: IUnitOfWork;

  @inject(Validator)
  @named(PostDeviceModel.name)
  private validator: IValidator<PostDeviceModel>;

  @inject(Symbols.serviceFor(DeviceModel))
  private deviceService!: IEntityService<DeviceModel>;

  public constructor(
    @inject(ILOGGERFACTORY) loggerFactory: ILoggerFactory,
  ) {
    super();
    this.logger = loggerFactory('post-device-handler');
  }

  @OpenApiOperationPost({
    path: '/api/v1/device',
    description: `Update Device`,
    summary: `Update Device`,
    request: {
      body: {
        description: `Device data`,
        required: true,
        schema: PostDeviceModel,
      }
    },
    responses: {
      204: {},
      400: {content: ErrorResponseModel},
      404: {content: ErrorResponseModel},
    }
  })
  public async handle(
    @requestBody() data: PostDeviceModel,
  ): Promise<void> {
    if (!data) {
      throw new BadRequestError('Post data is empty');
    }

    const valid = this.validator.validate(data, {requireRequired: true});
    if (valid !== true) {
      throw new BadRequestError(valid.join(', '));
    }

    this.pubsub.pub(`${data.tag}/manifest`, data.manifest);

    this.response
      .status(204)
      .end();
  }
}
