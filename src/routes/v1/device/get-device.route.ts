import { Route, httpGet, requestParam } from '@viatsyshyn/ts-rest';
import { AuthMiddleware } from '../../../middleware';
import { inject } from 'inversify';
import {WhereBuilder, OrderBuilder, and} from '@viatsyshyn/ts-orm';
import {IEntityService, Symbols} from '@viatsyshyn/ts-entity';
import { QueryParserMiddlewareFor } from '@viatsyshyn/ts-rest-query-to-orm';
import {COUNT_ONLY, LIMIT, OFFSET, SORT, ZoneModel} from '../../../models';
import {DeviceModel} from '../../../models';
import {AttributeModel} from '../../../models';
import {GetDeviceModel} from '../../../models';
import {DeviceResponseModel} from '../../../models';
import {OpenApiOperationGet} from '@viatsyshyn/ts-openapi';
import {ICache, ICACHE} from '../../../services/redis';

@httpGet('/api/v1/device', AuthMiddleware, QueryParserMiddlewareFor(GetDeviceModel))
export class GetDeviceRoute extends Route {

  @inject(ICACHE)
  private cache: ICache;

  @inject(Symbols.serviceFor(ZoneModel))
  private zoneService: IEntityService<ZoneModel>;

  @inject(Symbols.serviceFor(DeviceModel))
  private deviceService: IEntityService<DeviceModel>;

  @inject(Symbols.serviceFor(AttributeModel))
  private attributeService: IEntityService<AttributeModel>;

  @OpenApiOperationGet({
    path: '/api/v1/device',
    description: `Get devices`,
    summary: `Get devices`,
    request: {
      query: {
        sort: SORT,
        offset: OFFSET,
        limit: LIMIT,
        count: COUNT_ONLY,
      }
    },
    responses: {
      200: {content: [DeviceResponseModel]}
    }
  })
  public async handle(
    @requestParam('query') query: WhereBuilder<GetDeviceModel>,
    @requestParam('orderBy') orderBy: OrderBuilder<GetDeviceModel>,
    @requestParam('limit') limit: number,
    @requestParam('offset') offset: number,
    @requestParam('countOnly') countOnly: boolean
  ): Promise<void> {

    let baseQuery = this.deviceService.select()
      .where({forgotten: false});

    if (query) {
      baseQuery = baseQuery.where(query);
    }

    if (countOnly) {
      this.response.status(200).json(await baseQuery.count());
      return ;
    }

    const devices = await baseQuery
      .limit(limit)
      .offset(offset)
      .orderBy(orderBy)
      .fetch<DeviceModel>();

    if (devices.length < 1) {
      return this.json([]);
    }

    const ids = devices.map(({zone}) => zone);
    const zones = await this.zoneService.get(({id}) => id.in(ids));

    const response = await Promise.all(devices
      .map(async ({zone, tag, icons, widgets, id, ...rest}) => (
        {
          ...rest,
          tag,
          assets: icons,
          icon: widgets.icon,
          widgets: widgets.device,
          zone: zone ? zones.find(({id: _}) => _ === zone)!.uid : null,
          attributes: await this.cache.getAll(tag),
        }
      ))
    );

    return this.json<DeviceResponseModel[]>(response);
  }
}
