import {OpenApiPath} from '@viatsyshyn/ts-openapi';

export * from './get-device.route';
export * from './post-device.route';
export * from './put-device.route';

OpenApiPath({
  path: '/api/v1/device',
  tag: 'Device',
  security: [{ BearerAuth: [] }]
});
