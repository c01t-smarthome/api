import {httpGet, NotFoundError, queryParam, requestParam, Route} from '@viatsyshyn/ts-rest';
import {AuthMiddleware} from '../../../middleware';
import {OpenApiDataType, OpenApiOperationGet} from '@viatsyshyn/ts-openapi';
import {AttributeModel, DeviceModel, HistoryResponse} from '../../../models';
import {inject} from 'inversify';
import {IEntityService, Symbols} from '@viatsyshyn/ts-entity';
import {IStorage, ISTORAGE} from '../../../services/elastic';
import {UID} from '../../../models';

@httpGet('/api/v1/device/:uid/history', AuthMiddleware)
export class GetHistoryRoute extends Route {

  @inject(ISTORAGE)
  private storage: IStorage;

  @inject(Symbols.serviceFor(DeviceModel))
  private deviceService: IEntityService<DeviceModel>;

  @inject(Symbols.serviceFor(AttributeModel))
  private attributeService: IEntityService<AttributeModel>;

  @OpenApiOperationGet({
    path: '/api/v1/device/{device}/history',
    description: `Get device attribute history`,
    summary: `Get device attribute history`,
    request: {
      path: {
        device: UID,
      },
      query: {
        attributes: {
          description: `Attributes to fetch`,
          schema: {
            type: OpenApiDataType.array,
            items: OpenApiDataType.string,
            nullable: true,
            default: '*'
          },
        },
        since: {
          description: 'Starting since',
          schema: {
            type: OpenApiDataType.string,
            format: 'date-time',
            nullable: true,
          }
        },
        till: {
          description: 'Starting since',
          schema: {
            type: OpenApiDataType.string,
            format: 'date-time',
            nullable: true,
          }
        },
        interval: {
          description: 'Interval, eg 1s, 1m, 1d, 1w, 1M',
          schema: {
            type: OpenApiDataType.string,
          },
          required: true,
        },
      }
    },
    responses: {
      200: {content: [HistoryResponse]}
    }
  })
  public async handle(
    @requestParam('uid') uid: string | null,
    @queryParam('attributes') filter: string[] | null,
    @queryParam('since') since: string | null,
    @queryParam('till') till: string | null,
    @queryParam('interval') interval: string | null,
  ): Promise<void> {
    console.log('filter', filter);

    const queryAttributes = filter.filter(x => x && x !== '*');
    const sinceDate = new Date(since || 0);
    const tillDate = new Date(till || Date.now());

    console.log('GetHistoryRoute', queryAttributes, sinceDate, tillDate, interval);

    const device = await this.deviceService.getOne({uid});
    if (!device) {
      throw new NotFoundError(`Device ${JSON.stringify(uid)} not found`);
    }

    const attributes = (await this.attributeService.get({
        device: device.id,
        kind: 'out',
      }))
      .filter(({key}) => !queryAttributes.length || queryAttributes.indexOf(key) >= 0);


    const query: Array<[string, string]> = attributes.map(({key, definition}) => [key, definition.monotone ? 'avg' : 'sum']);
    const data = await this.storage.query<any>(device.tag, query, sinceDate, tillDate, interval);
    const model: HistoryResponse[] = attributes.map(({key, definition}) => ({
      attribute: key,
      definition,
      data: data.map(({timestamp, ...rest}) => ({
        timestamp,
        value: rest[key],
      }))
    }));

    return this.json(model);
  }
}
