import {OpenApiPath} from '@viatsyshyn/ts-openapi';

export * from './get-history.route';

OpenApiPath({
  path: '/api/v1/device/{device}/history',
  tag: 'History',
  security: [{ BearerAuth: [] }]
});
