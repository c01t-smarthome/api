import {
  BadRequestError,
  httpPut,
  requestBody,
  requestParam,
  Route,
} from '@viatsyshyn/ts-rest';
import {ILogger, ILoggerFactory, ILOGGERFACTORY} from '../../../services/logger';
import {inject, named} from 'inversify';
import {IEntityService, IUnitOfWork, Symbols} from '@viatsyshyn/ts-entity';
import {
  AttributeModel,
  AttributeResponse,
  ErrorResponseModel,
  PutAttributeModel,
  UID,
} from '../../../models';
import {IValidator, Validator} from '@viatsyshyn/ts-validate';
import {NotFoundError} from '@viatsyshyn/ts-orm';
import {AuthMiddleware} from '../../../middleware';
import {DeviceModel} from '../../../models';
import {OpenApiOperationPut} from '@viatsyshyn/ts-openapi';
import {ICache, ICACHE} from '../../../services/redis';
import {IPubSub, IPUBSUB} from '../../../services/mqtt';

@httpPut('/api/v1/device/:device/attribute/:uid', AuthMiddleware)
export class PutAttributeRoute extends Route {

  private readonly logger: ILogger;

  @inject(ICACHE)
  private cache!: ICache;

  @inject(IPUBSUB)
  private pubsub: IPubSub;

  @inject(Symbols.UnitOfWork)
  private unitOfWork!: IUnitOfWork;

  @inject(Validator)
  @named(PutAttributeModel.name)
  private validator: IValidator<PutAttributeModel>;

  @inject(Symbols.serviceFor(DeviceModel))
  private deviceService!: IEntityService<DeviceModel>;

  @inject(Symbols.serviceFor(AttributeModel))
  private attributeService!: IEntityService<AttributeModel>;

  public constructor(
    @inject(ILOGGERFACTORY) loggerFactory: ILoggerFactory,
  ) {
    super();
    this.logger = loggerFactory('put-attribute-handler');
  }

  @OpenApiOperationPut({
    path: '/api/v1/device/{device}/attribute/{uid}',
    description: `Update Device Attribute`,
    summary: `Update Device Attribute`,
    request: {
      path: {
        device: UID,
        uid: UID,
      },
      body: {
        description: `Device attribute data`,
        required: true,
        schema: PutAttributeModel,
      }
    },
    responses: {
      200: {content: AttributeResponse},
      400: {content: ErrorResponseModel},
      404: {content: ErrorResponseModel},
    }
  })
  public async handle(
    @requestParam('device') deviceUid: string,
    @requestParam('uid') uid: string,
    @requestBody() data: PutAttributeModel,
  ): Promise<void> {
    if (!data) {
      throw new BadRequestError('Put data is empty');
    }

    const valid = this.validator.validate(data, {requireRequired: true});
    if (valid !== true) {
      throw new BadRequestError(valid.join(', '));
    }

    const device = await this.deviceService.getOne({uid: deviceUid});
    if (!device) {
      throw new NotFoundError(`Device ${JSON.stringify(deviceUid)} not found`);
    }

    let attribute = await this.attributeService.getOne({uid});
    if (!attribute) {
      throw new NotFoundError(`Attribute ${JSON.stringify(uid)} not found`);
    }

    if (attribute.device !== device.id) {
      throw new BadRequestError(`Attribute does not belong to device`);
    }

    const model: Partial<AttributeModel> = JSON.parse(JSON.stringify({
      id: attribute.id,
      ...data,
      value: undefined,
    }));
    if (Object.keys(model).length > 1) {
      attribute = await this.unitOfWork.auto(async () =>
        this.attributeService.updateOne(model));
    }

    const {id, key, kind, device: _, ...rest} = attribute;

    if (data.value !== undefined) {
      await this.pubsub.pub(`${device.tag}/desired/${attribute.key}`, data.value);
    }

    return this.json({
      ...rest,
      key,
      kind,
      value: kind === 'out' ? await this.cache.get<any>(device.tag, key, null) : null,
    });
  }
}
