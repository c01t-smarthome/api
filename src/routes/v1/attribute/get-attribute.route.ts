import {Route, httpGet, requestParam, NotFoundError} from '@viatsyshyn/ts-rest';
import { AuthMiddleware } from '../../../middleware';
import { inject } from 'inversify';
import {WhereBuilder, OrderBuilder} from '@viatsyshyn/ts-orm';
import {IEntityService, Symbols} from '@viatsyshyn/ts-entity';
import { QueryParserMiddlewareFor } from '@viatsyshyn/ts-rest-query-to-orm';
import {AttributeResponse, COUNT_ONLY, GetAttributeModel, LIMIT, OFFSET, SORT, UID} from '../../../models';
import {DeviceModel} from '../../../models';
import {AttributeModel} from '../../../models';
import {OpenApiOperationGet} from '@viatsyshyn/ts-openapi';
import {ICache, ICACHE} from '../../../services/redis';

@httpGet('/api/v1/device/:uid/attribute', AuthMiddleware, QueryParserMiddlewareFor(GetAttributeModel))
export class GetAttributeRoute extends Route {

  @inject(ICACHE)
  private cache: ICache;

  @inject(Symbols.serviceFor(DeviceModel))
  private deviceService: IEntityService<DeviceModel>;

  @inject(Symbols.serviceFor(AttributeModel))
  private attributeService: IEntityService<AttributeModel>;

  @OpenApiOperationGet({
    path: '/api/v1/device/{device}/attribute',
    description: `Get device attributes`,
    summary: `Get device attributes`,
    request: {
      path: {
        device: UID
      },
      query: {
        sort: SORT,
        offset: OFFSET,
        limit: LIMIT,
        count: COUNT_ONLY,
      }
    },
    responses: {
      200: {content: [AttributeResponse]}
    }
  })
  public async handle(
    @requestParam('uid') uid: string,
    @requestParam('query') query: WhereBuilder<GetAttributeModel>,
    @requestParam('orderBy') orderBy: OrderBuilder<GetAttributeModel>,
    @requestParam('limit') limit: number,
    @requestParam('offset') offset: number,
    @requestParam('countOnly') countOnly: boolean
  ): Promise<void> {

    const device = await this.deviceService.getOne({uid});
    if (!device) {
      throw new NotFoundError(`Device ${JSON.stringify(uid)} not found`);
    }

    let baseQuery = this.attributeService.select()
      .where({device: device.id});

    if (query) {
      baseQuery = baseQuery.where(query);
    }

    if (countOnly) {
      this.response.status(200).json(await baseQuery.count());
      return ;
    }

    const attributes = await baseQuery
      .limit(limit)
      .offset(offset)
      .orderBy(orderBy)
      .fetch<AttributeModel>();

    if (attributes.length < 1) {
      return this.json([]);
    }

    const response = await Promise.all(attributes
      .map(async ({id, key, kind, device: _, ...rest}) => (
        {
          ...rest,
          key,
          kind,
          value: kind === 'out' ? await this.cache.get<any>(device.tag, key, null) : null,
        }
      ))
    );

    return this.json<AttributeResponse[]>(response);
  }
}
