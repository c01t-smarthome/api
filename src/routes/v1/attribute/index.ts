import {OpenApiPath} from '@viatsyshyn/ts-openapi';

export * from './get-attribute.route';
export * from './put-attribute.route';

OpenApiPath({
  path: '/api/v1/device/{device}/attribute',
  tag: 'Attribute',
  security: [{ BearerAuth: [] }]
});
