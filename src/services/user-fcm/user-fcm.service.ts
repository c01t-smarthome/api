import { UserFCMModel } from '../../models/db';
import { inject, injectable } from 'inversify';
import { IEntityService } from '@viatsyshyn/ts-entity';
import { and, or } from '@viatsyshyn/ts-orm';

export interface IUserFCMService {
  subscribeUserToFCM(inputData: UserFCMModel): Promise<UserFCMModel>;
}

@injectable()
export class UserFCMService implements IUserFCMService {

  constructor(
    @inject(Symbol.for('IEntityService<UserFCMModel>'))
    private readonly entityService: IEntityService<UserFCMModel>
  ) { }

  public async subscribeUserToFCM(inputData: UserFCMModel): Promise<UserFCMModel> {
    await this.entityService.delete(({token, deviceId, platform}) =>
      and(
        token.eq(inputData.token),
        or(deviceId.neq(inputData.deviceId), platform.neq(inputData.platform)))
    );
    return this.entityService.upsertOne(inputData);
  }
}
