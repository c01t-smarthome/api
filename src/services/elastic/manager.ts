import {Client} from 'elasticsearch';
import {inject, injectable} from 'inversify';
import {ILogger, ILOGGERFACTORY, ILoggerFactory} from '../logger';
import {IConfig} from 'config';

export const IELASTICMANAGER = Symbol.for('IElasticManager');

export interface IElasticManager {
  readonly client: Client;

  connect(): Promise<void>;
  disconnect(): Promise<void>;
}

@injectable()
export class ElasticManager implements IElasticManager {
  get client(): Client {
    if (!this.instance) {
      throw new Error('Elastic not ready, call connect() first');
    }
    return this.instance!;
  }

  private instance: Client | null = null;
  private logger: ILogger;

  public constructor(
    @inject(ILOGGERFACTORY) loggerFactory: ILoggerFactory,
    @inject(Symbol.for('IConfig')) private config: IConfig,
  ) {
    this.logger = loggerFactory('elastic');
  }

  async connect(): Promise<void> {
    this.logger.info(`Start Elastic connection`);
    this.instance = new Client({host: this.config.get('elastic.uri') });
    while (true) {
      try {
        await this.instance.ping({});
        break;
      } catch {
        await new Promise(resolve => setTimeout(resolve, 1000 ));
        // ignored
      }
    }

    this.logger.info('Elastic initialized');
  }

  async disconnect(): Promise<void> {
    this.client.close();
  }
}
