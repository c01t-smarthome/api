import { injectable } from 'inversify';
import * as crypto from 'crypto';
import { ICryptoService } from '.';

const ALGORITHM = 'sha256';

export interface ICryptoService {
  encode(data: string, salt?: string): string;
  verify(encoded: string, data: string, salt?: string, ): boolean;
}

@injectable()
export class CryptoService implements ICryptoService {

  public encode(data: string, salt?: string): string {
    const hash = salt !== undefined
      ? crypto.createHmac(ALGORITHM, salt)
      : crypto.createHash(ALGORITHM);
    return hash.update(data).digest('hex').toString();
  }

  public verify(encoded: string, data: string, salt?: string): boolean {
    return encoded === this.encode(data, salt);
  }
}
