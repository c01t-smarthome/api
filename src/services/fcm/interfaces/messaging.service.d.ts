import { INotification, IMessageData, Priority } from './message';
import { IMessagingResult } from './messaging-result';

export interface IMessagingService {
  sendMessage<T extends IMessageData>(
    tokens: string[],
    data: T,
    priority?: Priority,
    timeToLive?: number,
  ): Promise<IMessagingResult[]>;

  sendPushNotification(
    tokens: string[],
    notification: INotification,
    priority?: Priority,
    timeToLive?: number,
  ): Promise<IMessagingResult[]>;

  send<T extends IMessageData>(
    tokens: string[],
    data?: T,
    notification?: INotification,
    priority?: Priority,
    timeToLive?: number,
  ): Promise<IMessagingResult[]>;
}
