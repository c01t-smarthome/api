type Priority = 'normal' | 'high'

export interface IMessage {
  to: string;
  priority?: Priority;
  notification?: INotification;
  data?: IMessageData;
  content_available?: boolean;
  ttl?: number;
}

export interface INotification {
  title: string;
  body: string;
  icon: string;
}

export interface IMessageData {
  topic: string;
}
