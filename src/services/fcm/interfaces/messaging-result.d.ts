export interface IMessagingResult {
  multicast_id: number;
  success: number;
  failure: number;
  results: Array<{
    message_id: string;
    error: string;
  }>;
}
