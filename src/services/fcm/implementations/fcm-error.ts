import { IMessagingResult } from '../interfaces';

export class FcmError extends Error {
  public type: string;
  public fcmMessagingResult: IMessagingResult;
  public token: string;
  constructor(token: string, message?: string, type?: string, messageResult?: IMessagingResult) {
    super();
    this.name = 'FCM_ERROR';
    this.message = message || 'Fcm error';
    this.type = type;
    this.fcmMessagingResult = messageResult;
    this.token = token;
    Object.setPrototypeOf(this, FcmError.prototype);
  }
}
