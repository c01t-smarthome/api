import {
  IMessagingService,
  INotification,
  IMessage,
  IMessageData,
  IMessagingResult,
  Priority
} from '../interfaces';
import { injectable, inject } from 'inversify';

import { BadRequestError } from '@viatsyshyn/ts-rest';
import {ILogger, ILOGGERFACTORY, ILoggerFactory} from '../../logger';
import { FcmError } from './fcm-error';
import {IConfig} from 'config';

const FCM = require('fcm-node');


@injectable()
export class FCMService implements IMessagingService {

  private logger: ILogger;

  constructor(
    @inject(Symbol.for('IConfig'))
    private readonly config: IConfig,
    @inject(ILOGGERFACTORY) loggerFactory: ILoggerFactory,
  ) {
    this.logger = loggerFactory('fcm-service');
  }

  public sendMessage<T extends IMessageData>(
    tokens: string[],
    data: T,
    priority: Priority = 'normal',
    ttl?: number,
  ): Promise<IMessagingResult[]> {
    return this.send(tokens, data, undefined, priority, ttl);
  }

  public sendPushNotification(
    tokens: string[],
    notification: INotification,
    priority: Priority = 'normal',
    ttl?: number,
  ): Promise<IMessagingResult[]> {
    return this.send(tokens, null, notification, priority, ttl);
  }

  public send<T extends IMessageData>(
    tokens: string[],
    data?: T,
    notification?: INotification,
    priority: Priority = 'normal',
    ttl?: number,
  ): Promise<IMessagingResult[]> {

    if (!tokens || tokens.length === 0) {
      throw new BadRequestError('tokens list is empty');
    }

    if (!data && !notification) {
      throw new BadRequestError('Message is empty. Data or notification is required');
    }

    const client = new FCM(this.config.get<string>('fcm.server-key'));

    const promises = tokens.map(token => {

      const message: IMessage = {
        to: token,
        data,
        notification,
        priority,
        content_available: data != null,
        ttl
      };

      // tslint:disable-next-line: no-console
      console.log('FCM Message', JSON.stringify(message, null, 2));

      return new Promise<IMessagingResult>((resolve, reject) => {
        client.send((message), (strErr: string, res: any) => {
          if (strErr) {
            let fcmError: FcmError;

            if (strErr === 'InvalidServerResponse') {
              fcmError = new FcmError(token, strErr, strErr);
            } else {
              const obj: IMessagingResult = JSON.parse(strErr);
              if (Array.isArray(obj.results) && obj.results[0]) {
                const msg = obj.results[0].error;
                fcmError = new FcmError(token, msg, msg, obj);
              } else {
                fcmError = new FcmError(token);
              }
            }
            this.logger.error(fcmError);
            reject(fcmError);
          } else {
            this.logger.info('Send Result ' + res);
            resolve(JSON.parse(res));
          }
        });
      });
    });
    return Promise.all(promises);
  }
}
