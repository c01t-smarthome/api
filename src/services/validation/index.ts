import {Validator, Constraint} from '@viatsyshyn/ts-validate';

const validateJs: any = require('validate.js');

import {register as registerDefault, SingleValidator} from '@viatsyshyn/ts-validate';

import {UserType} from '../../models/db';
import {Container} from 'inversify';
import moment = require('moment');

export const DEFAULT_TIME_FORMAT = 'HH:mm:ss.SSSZ';
export const SUPPORTED_TIME_FORMATS = [
  'HH:mm:ss.SSSZ',
  'HH:mm:ss.SSS',
  'HH:mm:ssZ',
  'HH:mm:ss',
];

validateJs.validators.time = (value: any) => {
  // Empty values are allowed. Use presence validator along with this validator for required time property.

  if (!validateJs.isDefined(value)) {
    return;
  }

  const error = `Time ${value} is invalid. Time should have next format hh:mm:ss.SSS (for example 12:10:20)`
  if (typeof (value) !== 'string') {
    return error;
  }

  if (!moment(value, SUPPORTED_TIME_FORMATS, true).isValid()) {
    return error
  }
};

export { IValidator, validate, IValidationOptions, ModelValidator } from '@viatsyshyn/ts-validate';

export function register(container: Container) {
  registerDefault(container);
  const constraints = getConstraints();
  Object.keys(constraints).forEach(key => {
    container.bind(Constraint).toConstantValue(constraints[key]).whenTargetNamed(key);
    container.bind(Validator).toConstantValue(new SingleValidator(container, key)).whenTargetNamed(key);
  });
}

export const SingleValidatorNames = Object.freeze({
  PositiveInt: 'positiveInt',
  Date: 'date',
  Time: 'time',
  UserType: 'userType'
});

function getConstraints(): {[key: string]: any} {
  return {
    time: {
      time: {}
    },
    date: {
    },
    positiveInt: {
      numericality: {
        noStrings: true,
        onlyInteger: true,
        greaterThanOrEqualTo: 0,
        // lessThanOrEqualTo
      }
    },
    userType: {
      inclusion: {
        within: [UserType.Unknown, UserType.Admin, UserType.User],
        message: `Invalid user type`,
      }
    }
  }
}
