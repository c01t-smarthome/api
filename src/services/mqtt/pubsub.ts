import { injectable, inject } from 'inversify';
import {ILogger, ILoggerFactory, ILOGGERFACTORY} from '../logger';
import {IMqttManager, IMQTTMANAGER} from './manager';

export type TMessageHandler<T = any> = (msg: T, topic: string) => void;

export interface IPubSub {
  sub<T = any>(topic: string, cb: TMessageHandler<T>): this;
  unsub(topic: string, cb: any): this;
  pub<T = any>(topic: string, msg: T): this;
}

export const IPUBSUB = Symbol.for('IPubSub');

const mqtt_regex = require('mqtt-regex');

@injectable()
export class PubSub implements IPubSub {
  private subscriptions = new Set<string>();
  private handlers: Array<[string, Function]> = [];
  private logger: ILogger;

  constructor(
    @inject(ILOGGERFACTORY) factory: ILoggerFactory,
    @inject(IMQTTMANAGER) private manager: IMqttManager,
  ) {
    this.logger = factory('pubsub');

    this.manager.client
      .on('message', (t, m) => {
        this.handlers.forEach(([topic, cb]) => {
          const regex = mqtt_regex(topic);
          if (regex.exec(t)) {
            setImmediate(() => cb(JSON.parse(m.toString()), t));
          }
        })
      });
  }

  unsub(topic: string, cb: any): this {
    this.handlers = this.handlers.filter(([t, c]) => t !== topic && c !== cb);
    return this;
  }

  sub<T = any>(topic: string, cb: TMessageHandler<T>): this {
    this.logger.log(`Subscribe ${topic}`);
    const subs = this.subscriptions;
    if (!subs.has(topic)) {
      this.manager.client.subscribe(topic);
      subs.add(topic);
    }

    this.handlers.push([topic, cb]);

    return this;
  }

  pub<T = any>(topic: string, msg: T): this {
    let data = JSON.stringify(msg);
    this.logger.log(`Publish ${topic} ${data}`);
    this.manager.client.publish(topic, data);
    return this;
  }
}
