import { inject, injectable } from 'inversify';
import { IUserService } from '.';
import { UserModel } from '../../models/db';
import { NotFoundError, BadRequestError } from '@viatsyshyn/ts-rest';
import { ICryptoService } from '../crypto';

import { DuplicateValueDbError } from '@viatsyshyn/ts-orm-pg';
import { IEntityService } from '@viatsyshyn/ts-entity';

export interface IUserService {

  authenticate(username: string, password: string): Promise<UserModel | null>;

  addOne(user: UserModel): Promise<UserModel>;
  updateOne(user: Partial<UserModel>): Promise<UserModel>;
  getOne(criteria: Partial<UserModel>): Promise<UserModel | null>;
}


@injectable()
export class UserService implements IUserService {

  private static getSalt(user: UserModel): string {
    return user.created.toISOString();
  }

  constructor(
    @inject(Symbol.for('IEntityService<UserModel>'))
    private entityService: IEntityService<UserModel>,
    @inject(Symbol.for('ICryptoService'))
    private readonly crypto: ICryptoService
  ) {
  }

  public async addOne(user: UserModel): Promise<UserModel> {
    try {
      user = await this.entityService.addOne(user);
      const encodedPassword = this.crypto.encode(user.password, UserService.getSalt(user));
      return await this.entityService.updateOne({ id: user.id, password: encodedPassword });
    } catch (err) {
      if (err instanceof DuplicateValueDbError) {
        throw new BadRequestError(`Key '${err.value}' already exists for field '${err.column}'.`);
      }
      throw err;
    }
  }

  public async updateOne(user: Partial<UserModel>): Promise<UserModel> {
    const oldUser = await this.getById(user.id);

    if (!oldUser) {
      return Promise.reject(new NotFoundError(`User not found`));
    }

    if (user.password) {
      user.password = this.crypto.encode(user.password, UserService.getSalt(oldUser));
    }

    try {
      return await this.entityService.updateOne(user);
    } catch (err) {
      if (err instanceof DuplicateValueDbError) {
        throw new BadRequestError(`Key '${err.value}' already exists for field '${err.column}'.`);
      }
      throw err;
    }
  }

  public async authenticate(username: string, password: string): Promise<UserModel | null> {
    const user = await this.getByUserName(username);
    if (!user) {
      return null;
    }
    const verified = this.crypto.verify(user.password, password, UserService.getSalt(user));
    return verified ? user : null;
  }

  public getOne(criteria: Partial<UserModel>): Promise<UserModel | null> {
    return this.entityService.getOne(criteria);
  }

  private getById(id: number): Promise<UserModel | undefined> {
    return this.entityService.getOne({id});
  }

  private getByUserName(userName: string): Promise<UserModel | undefined> {
    if (!userName || userName.trim() === '') {
      throw new Error('Parameter userName is required');
    }
    return this.entityService.getOne({userName});
  }
}
