import {inject, injectable} from 'inversify';
import {ILogger, ILOGGERFACTORY, ILoggerFactory} from '../logger';
import {IConfig} from 'config';
import {parseConnection} from '../../utils';
import {Tedis} from 'tedis';

export const IREDISMANAGER = Symbol.for('IRedisManager');

export interface IRedisManager {
  readonly client: Tedis;

  connect(): Promise<void>;
  disconnect(): Promise<void>;
}

@injectable()
export class RedisManager implements IRedisManager {
  get client(): Tedis {
    if (!this.instance) {
      throw new Error('Mqtt not ready, call connect() first');
    }
    return this.instance!;
  }

  private instance: Tedis | null = null;
  private logger: ILogger;

  public constructor(
    @inject(ILOGGERFACTORY) loggerFactory: ILoggerFactory,
    @inject(Symbol.for('IConfig')) private config: IConfig,
  ) {
    this.logger = loggerFactory('redis');
  }

  connect(): Promise<void> {
    this.logger.info(`Start Redis connection`);
    return new Promise<void>((resolve) => {
      this.instance = new Tedis(parseConnection(this.config.get('redis.uri')));
      this.instance.on('connect', () => {
        this.logger.info('Redis initialized');
        resolve();
      });
    })
  }

  async disconnect(): Promise<void> {
  }
}
