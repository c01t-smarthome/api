import { injectable, inject } from 'inversify';
import { IEntityService } from '@viatsyshyn/ts-entity';
import { IMessagingService, IMessageData, FcmError } from '../fcm';
import { INotification } from '../../services';
import {UserFCMModel} from '../../models/db';

export interface INotificationService {
}

@injectable()
export class NotificationService implements INotificationService {

  constructor(
    @inject(Symbol.for('IEntityService<UserFCMModel>'))
    private readonly userFcmEntityService: IEntityService<UserFCMModel>,
    @inject(Symbol.for('IMessagingService'))
    private readonly fcmClient: IMessagingService,
  ) {
  }

  private async sendPushNotification(tokens: string[], notification: INotification): Promise<void> {
    try {
      await this.fcmClient.sendPushNotification(tokens, notification);
    } catch (e) {
      if (e instanceof FcmError && e.type === 'MismatchSenderId') {
        await this.userFcmEntityService.delete({token: e.token});
      }
      throw e;
    }
  }

  private async sendMessage<T extends IMessageData >(tokens: string[], data: T): Promise<void> {
    try {
      await this.fcmClient.sendMessage<T >(tokens, data);
    } catch (e) {
      if (e instanceof FcmError && e.type === 'MismatchSenderId') {
        await this.userFcmEntityService.delete({token: e.token});
      }
      throw e;
    }
  }

  private getListOfUserFcm(userIds: number[]): Promise<UserFCMModel[]> {
    return this.userFcmEntityService.get(({userId}) => userId.in(userIds));
  }

  private async getTokens(userIds: number[]): Promise<string[]> {
    return (await this.getListOfUserFcm(userIds)).map(uFcm => uFcm.token);
  }
}
