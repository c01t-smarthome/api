export interface ILogger {
  log(...args: any[]): void;
  info(...args: any[]): void;
  error(...args: any[]): void;
}

export type ILoggerFactory = (ns: string) => ILogger;

export const ILOGGERFACTORY = Symbol.for('ILoggerFactory');

export function LoggerFactory(ns: string): ILogger {
  return <ILogger>{
    log: (...args: any[]) => {
      console.log(`${ns}:log`, ...args);
    },
    info: (...args: any[]) => {
      console.log(`${ns}:info`, ...args);
    },
    error: (...args: any[]) => {
      console.error(`${ns}:error`, ...args);
    }
  };
}
