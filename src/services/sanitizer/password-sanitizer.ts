export type WithPassword = {password: any};
export type NoPassword<T> = Omit<T, 'password'>;

export const PasswordSanitizer = <T extends WithPassword, X extends NoPassword<T>>(x: T): X => {
  delete x.password;
  return x as any;
};
