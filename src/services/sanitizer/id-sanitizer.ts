export type WithId = {id: any};
export type NoId<T> = Omit<T, 'id'>;

export const IdSanitizer = <T extends WithId, X extends NoId<T>>(x: T): X => {
  delete x.id;
  return x as any;
};
