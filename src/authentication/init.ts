import * as express from 'express';
import * as passport from 'passport';
import {Strategy, ExtractJwt} from 'passport-jwt';
import {Container} from 'inversify';
import {IJWTCONFIG, IJwtConfig} from './jwt-config';

export function init(app: express.Application, container: Container) {

  const jwtConfig = container.get<IJwtConfig>(IJWTCONFIG);

  const opts = {
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: jwtConfig.secret
  };

  passport.use(new Strategy(opts, (jwtPayload, done) => done(null, jwtPayload || null)));

  app.use(passport.initialize());

  const authenticator = passport.authenticate('jwt', { session: false, failWithError: true });
  app.use((req: express.Request, res: express.Response, next: express.NextFunction) => {
    // ignore jwt error, auth middleware will take care of that
    authenticator(req, res, () => next());
  });
}
