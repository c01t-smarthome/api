import { injectable, inject } from 'inversify';

import { IConfig } from 'config';

export const IJWTCONFIG = Symbol.for('IJwtConfig');

export interface IJwtConfig {
  secret: string;
}

@injectable()
export class JwtConfig implements IJwtConfig {
  public secret: string;

  constructor(
    @inject(Symbol.for('IConfig')) config: IConfig
  ) {
    const configData = config.get<IJwtConfig>('jwt');
    this.secret = configData.secret;
  }
}
