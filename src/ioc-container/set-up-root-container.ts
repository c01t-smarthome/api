import { Container } from 'inversify';

// SERVICES
import {ILOGGERFACTORY, ILoggerFactory, LoggerFactory} from '../services/logger';

import {ErrorHandlerMiddleware} from '../middleware';

import * as config from 'config';

import {IJWTCONFIG, IJwtConfig, JwtConfig} from '../authentication';

import {Pool, PoolConfig} from 'pg';
import {ISqlQuery, PgCompiler, PgDriver} from '@viatsyshyn/ts-orm-pg';
import {DbReader, IBuildableQueryCompiler, IDbDataReader, ISqlDataDriver} from '@viatsyshyn/ts-orm';
import {Symbols as EntitySymbols} from '@viatsyshyn/ts-entity';
import {Validator} from '@viatsyshyn/ts-validate';

import {FCMService, IMessagingService} from '../services/fcm';
import {IMQTTMANAGER, IMqttManager, MqttManager} from '../services/mqtt';
import {IREDISMANAGER, IRedisManager, RedisManager} from '../services/redis';
import {ElasticManager, IELASTICMANAGER, IElasticManager} from '../services/elastic';
import {IdSanitizer, PasswordSanitizer, WithId, WithPassword} from '../services/sanitizer';
import {IValidator, ModelValidator, register } from '../services/validation';
import {CryptoService, ICryptoService} from '../services/crypto';

import {AttributeModel, DeviceModel, UserFCMModel, UserModel, ZoneModel} from '../models/db';
import {
  PostUserFcmModel,
  PostZoneModel,
  PutAttributeModel,
  PostDeviceModel,
  PutDeviceModel,
  PutZoneModel,
  PutUserModel,
  SignUpModel
} from '../models/input';
import {Sanitizer, Symbols as RestSymbols} from '@viatsyshyn/ts-rest';

export function setUpRootContainer(): Container {

  const container = new Container();

  container.bind<config.IConfig>(Symbol.for('IConfig')).toConstantValue(config);
  // BIND JWT
  container.bind<IJwtConfig>(IJWTCONFIG).to(JwtConfig).inSingletonScope();
  // BIND LOGGER
  container.bind<ILoggerFactory>(ILOGGERFACTORY).toConstantValue(LoggerFactory);

  // BIND FCM
  container.bind<IMessagingService>(Symbol.for('IMessagingService')).to(FCMService).inSingletonScope();
  // BIND crypto
  container.bind<ICryptoService>(Symbol.for('ICryptoService')).to(CryptoService).inSingletonScope();

  // BIND DB
  const sqlConfig = config.get<PoolConfig>('dbConfig');
  const pool = new Pool(sqlConfig);
  container.bind<ISqlDataDriver<ISqlQuery>>(EntitySymbols.ISqlDataDriver).toConstantValue(new PgDriver(pool));
  container.bind<IBuildableQueryCompiler<ISqlQuery>>(EntitySymbols.ISqlQueryCompiler).toConstantValue(new PgCompiler());

  // BIND MANAGERS
  container.bind<IMqttManager>(IMQTTMANAGER).to(MqttManager).inSingletonScope();
  container.bind<IRedisManager>(IREDISMANAGER).to(RedisManager).inSingletonScope();
  container.bind<IElasticManager>(IELASTICMANAGER).to(ElasticManager).inSingletonScope();

  [
    UserModel,
    UserFCMModel,
    ZoneModel,
    DeviceModel,
    AttributeModel,
  ].forEach(Model => {
    container.bind<IDbDataReader<any>>(EntitySymbols.dbReaderFor(Model))
      .toConstantValue(new DbReader<any>(Model));
  });

  register(container); // register constraints;
  [
    SignUpModel,
    PutUserModel,
    PostUserFcmModel,
    PostZoneModel,
    PutZoneModel,
    PostDeviceModel,
    PutDeviceModel,
    PutAttributeModel,
  ].forEach(Model => {
    container.bind<IValidator<any>>(Validator)
      .toConstantValue(new ModelValidator<any>(Model, container))
      .whenTargetNamed(Model.name);
  });

  // BIND SANITIZERS
  container.bind<Sanitizer<WithId>>(RestSymbols.Sanitizer)
    .toConstantValue(IdSanitizer)
    .whenTargetNamed('id');
  container.bind<Sanitizer<WithPassword>>(RestSymbols.Sanitizer)
    .toConstantValue(PasswordSanitizer)
    .whenTargetNamed('password');

  // BIND error middleware
  container.bind<ErrorHandlerMiddleware>(Symbol.for('ErrorHandler')).to(ErrorHandlerMiddleware);

  return container;
}
