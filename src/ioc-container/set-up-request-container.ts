import { Container } from 'inversify';
import {Request} from 'express';

import {ISqlQuery} from '@viatsyshyn/ts-orm-pg';
import {IBuildableQueryCompiler, IDbDataReader} from '@viatsyshyn/ts-orm';
import {
  EntityService,
  IEntityService,
  IQueryExecutorProvider,
  IUnitOfWork,
  QueryExecutorProvider,
  Symbols,
  UnitOfWork
} from '@viatsyshyn/ts-entity';

import {
  Cache,
  ICACHE,
  ICache,
  INotificationService, IPUBSUB, IPubSub,
  IStorage,
  ISTORAGE,
  NotificationService, PubSub,
  Storage
} from '../services';
import {IUserService, UserService} from '../services';
import {IUserFCMService, UserFCMService} from '../services';

import {AttributeModel, DeviceModel, UserFCMModel, UserModel, ZoneModel} from '../models/db';

function bindServices(container: Container, ...entityClasses: Array<InstanceType<any>>): void {

  // bind entityServices
  entityClasses.forEach(Model => {
    const executorProvider = container.get<IQueryExecutorProvider<ISqlQuery>>(Symbols.QueryExecutorProvider);
    const queryBuilder = container.get<IBuildableQueryCompiler<ISqlQuery>>(Symbols.ISqlQueryCompiler);
    const dbDataRead = container.get<IDbDataReader<any>>(Symbols.dbReaderFor(Model));
    container
      .bind<IEntityService<any>>(Symbols.serviceFor(Model))
      .toConstantValue(new EntityService<any, ISqlQuery>(Model, executorProvider, queryBuilder, dbDataRead));
  });

  container
    .bind<IUserService>(Symbol.for('IUserService'))
    .to(UserService)
    .inSingletonScope();

  container
    .bind<IUserFCMService>(Symbol.for('IUserFCMService'))
    .to(UserFCMService)
    .inSingletonScope();

  container
    .bind<INotificationService>(Symbol.for('INotificationService'))
    .to(NotificationService)
    .inSingletonScope();

  container.bind<IStorage>(ISTORAGE).to(Storage).inSingletonScope();
  container.bind<ICache>(ICACHE).to(Cache).inSingletonScope();
  container.bind<IPubSub>(IPUBSUB).to(PubSub).inSingletonScope();
}


export function setUpRequestContainer(container: Container, req: Request) {

  container.bind<IUnitOfWork>(Symbols.UnitOfWork).to(UnitOfWork).inSingletonScope();
  container.bind<IQueryExecutorProvider<ISqlQuery>>(Symbols.QueryExecutorProvider).to(QueryExecutorProvider);

  bindServices(container,
    UserModel,
    UserFCMModel,
    ZoneModel,
    DeviceModel,
    AttributeModel,
  );

  container.bind<UserModel>(Symbol.for('CurrentUser')).toConstantValue((req.user as UserModel) || null);
}
