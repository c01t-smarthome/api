import 'reflect-metadata';
import { setUpRequestContainer } from './ioc-container';

import * as bodyParser from 'body-parser';
import * as express from 'express';
import * as http from 'http';

const compression = require('compression');
const helmet = require('helmet');

import { Container } from 'inversify';

import { IConfig } from 'config';

import expressServerBuilder, { IHttpRequest, IHttpResponse, IErrorMiddleware } from '@viatsyshyn/ts-rest';
import {openApi, OpenApiHttpSecurityScheme} from '@viatsyshyn/ts-openapi';

import {ILogger, ILOGGERFACTORY, ILoggerFactory} from './services/logger';
import {IElasticManager, IELASTICMANAGER, IMQTTMANAGER, IMqttManager, IREDISMANAGER} from './services';
import {IRedisManager} from './services';
import {init as authInit} from './authentication';

export class ApplicationServer {

  private logger: ILogger;
  private expApp: express.Application;
  private server: http.Server;
  private port = 3000;
  private mqtt: IMqttManager;
  private redis: IRedisManager;
  private elastic: IElasticManager;

  constructor(container: Container) {

    this.logger = container.get<ILoggerFactory>(ILOGGERFACTORY)('server');

    this.expApp = this.createExpressApplication(container);

    this.mqtt = container.get<IMqttManager>(IMQTTMANAGER);
    this.redis = container.get<IRedisManager>(IREDISMANAGER);
    this.elastic = container.get<IElasticManager>(IELASTICMANAGER);

    const config = container.get<IConfig>(Symbol.for('IConfig'));

    if (config.has('port')) {
      this.port = config.get('port');
    }
  }

  public async start(): Promise<void> {

    await Promise.all([
      this.mqtt.connect(),
      this.redis.connect(),
      this.elastic.connect(),
    ]);

    this.server = await new Promise((resolve, reject) => {
      try {
        const server = this.expApp.listen(this.port, '0.0.0.0');
        server.addListener('error', (e) => {
          this.logger.error('Server start failed: ' + (e.message || e));
          reject(e);
        });
        server.addListener('listening', () => {
          resolve(server);
        })
      } catch (e) {
        this.logger.error('Server start failed: ' + (e.message || e));
        reject(e);
      }
    });

    this.logger.info(`API started on ${this.port}`);
  }

  public stop(): void {
    this.server.close();
    this.mqtt.disconnect();
    this.redis.disconnect();
    this.elastic.disconnect();
  }

  private createExpressApplication(container: Container): express.Application {
    const app = express();

    app.use(bodyParser.urlencoded({extended: true}));
    app.use(bodyParser.json());
    app.use(bodyParser.raw());
    app.use(bodyParser.text());

    app.use(compression());

    app.use(helmet.xssFilter());
    app.use(helmet.frameguard());

    if (process.env.NODE_ENV !== 'production') {
      app.use(openApi({
        info: {
          info: {
            title: 'Smarthome API',
            version: '1.0.1'
          },
          components: {
            securitySchemes: {
              BearerAuth: {
                type: 'http',
                scheme: OpenApiHttpSecurityScheme.bearer,
                bearerFormat: 'JWT',
              }
            }
          },
        }
      }));
    }

    authInit(app, container);

    expressServerBuilder(app, container, setUpRequestContainer);

    app.use((err: Error, req: IHttpRequest, res: IHttpResponse, next: express.NextFunction) => {
      container.get<IErrorMiddleware>(Symbol.for('ErrorHandler'))
        .handle(err, req, res)
        .then(next);
    });

    return app;
  }
}
