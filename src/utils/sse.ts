import {injectable} from 'inversify';
import {IHttpRequest, IHttpResponse, IMiddleware} from '@viatsyshyn/ts-rest';

export type SseField = 'event' | 'data' | 'id' | 'retry'; // keyof fieldBufs

export type SseValue = Buffer | any;

export type SseSerializer = (value: any) => string | Buffer;

export interface ISseBlockConfiguration extends Partial<Record<SseField, SseValue>> {
}

const fieldBuffers = {
  __comment__: Buffer.from(': '),
  event: Buffer.from('event: '),
  data: Buffer.from('data: '),
  id: Buffer.from('id: '),
  retry: Buffer.from('retry: ')
};

const eolBuf = Buffer.from('\n');

const stringSerialize: SseSerializer = String;
const jsonSerialize: SseSerializer = JSON.stringify;

/**
 * Creates a Buffer for a SSE "instruction" -- `event: myEvent\n`
 *
 * @param field The instruction field
 * @param value The instruction value
 * @param serializer Value serializer for `data`
 */
export function instruction(field: SseField, value: SseValue, serializer?: SseSerializer): Buffer {
  return Buffer.concat([
    fieldBuffers[field], toBuffer(value, serializer), eolBuf
  ]);
}

/**
 * Creates a Buffer for a SSE comment -- `: this is a comment\n`
 *
 * @param comment The comment message
 */
export function comment(comment: string): Buffer { // tslint:disable-line:no-shadowed-variable
  return instruction('__comment__' as SseField, comment, stringSerialize);
}

/**
 * Creates a Buffer for a SSE block of instructions -- event: myEvent\ndata: "eventData"\n\n
 *
 * @param instructions An object map of SSEFields to SSEValues
 * @param serializer Value serializer for `data`
 */
export function block(instructions: ISseBlockConfiguration, serializer?: SseSerializer): Buffer {
  const lines = Object.keys(instructions).map((field: SseField) => {
    const fieldSerializer = field === 'data' ? serializer : stringSerialize;

    return instruction(
      field as SseField,
      toBuffer(instructions[field], fieldSerializer)
    );
  });

  return Buffer.concat([...lines, eolBuf]);
}

/**
 * Create a buffer for a standard SSE block composed of `event`, `data`, and `id` (only `data` is mandatory).
 * To create a data-only message (without event name), pass `null` to `event`.
 *
 * @param event The event name, null to create a data-only message
 * @param data The event data
 * @param id The event ID
 * @param serializer Value serializer for `data`
 */
export function message(
  event: string | null,
  data: SseValue,
  id?: string,
  serializer?: SseSerializer
): Buffer {
  const frame: ISseBlockConfiguration = {};

  if (id != null) {
    frame.id = id;
  }

  if (event != null) {
    frame.event = event;
  }

  if (data === undefined) {
    throw new Error('The `data` field in a message is mandatory');
  }

  frame.data = data;

  return block(frame, serializer);
}

/**
 * Applies the serializer on a value then converts the resulting string in an UTF-8 Buffer of characters.
 *
 * @param value The value to serialize
 * @param serializer Value serializer
 */
function toBuffer(value: SseValue, serializer: SseSerializer = jsonSerialize) {
  if (Buffer.isBuffer(value)) {
    return value;
  }

  const serialized = serializer(value);

  return Buffer.isBuffer(serialized)
    ? serialized
    : Buffer.from(serialized);
}

export interface ISseMiddlewareOptions {
  /**
   * Serializer function applied on all messages' data field (except when you direclty pass a Buffer).
   * SSE comments are not serialized using this function.
   *
   * @default JSON.stringify
   */
  serializer: SseSerializer;

  /**
   * Whether to flush headers immediately or wait for the first res.write().
   *  - Setting it to false can allow you or 3rd-party middlewares to set more headers on the response.
   *  - Setting it to true is useful for debug and tesing the connection, ie. CORS restrictions fail only when headers
   *    are flushed, which may not happen immediately when using SSE (it happens after the first res.write call).
   *
   * @default true
   */
  flushHeaders: boolean;

  /**
   * Determines the interval, in milliseconds, between keep-alive packets (neutral SSE comments).
   * Pass false to disable heartbeats (ie. you only support modern browsers/native EventSource implementation and
   * therefore don't need heartbeats to avoid the browser closing an inactive socket).
   *
   * @default 5000
   */
  keepAliveInterval: false | number;

  /**
   * If you are using expressjs/compression, you MUST set this option to true.
   * It will call res.flush() after each SSE messages so the partial content is compressed and reaches the client.
   * Read {@link https://github.com/expressjs/compression#server-sent-events} for more.
   *
   * @default false
   */
  flushAfterWrite: boolean;
}


export interface ISseFunctions {
  /**
   * Writes a standard SSE data message on the socket.
   *
   * Client example:
   *     const ev = new EventSource('/sse');
   *     ev.addEventListener('message', event => console.log(event.data)); // recommended
   *     ev.onmessage = event => console.log(event.data); // legacy way
   *
   * @param data The event data1
   * @param id The event ID, useful for replay thanks to the Last-Event-ID header
   */
  pushData(data: SseValue, id?: string): void;

  /**
   * Writes a standard SSE message (with named event) on the socket.
   *
   * Client example:
   *     const ev = new EventSource('/sse');
   *     ev.addEventListener('evname', event => console.log(event.data));
   *
   * @param event The event name
   * @param data The event data (mandatory!)
   * @param id The event ID, useful for replay thanks to the Last-Event-ID header
   */
  pushEvent(event: string, data: SseValue, id?: string): void;

  /**
   * Writes a standard SSE comment on the socket.
   * Comments are informative and useful for debugging. There are discarded by EventSource on the browser.
   *
   * @param comment The comment message (not serialized)
   */
  pushComment(comment: string): void;
}

/**
 * An ISseResponse is an augmented Express response that contains an `sse` property that contains various
 * functions (data, event and comment) to send SSE messages.
 */
export interface ISseResponse extends IHttpResponse, ISseFunctions {
}

@injectable()
export class SSEMiddleware implements IMiddleware {

  private static config: ISseMiddlewareOptions = {
    keepAliveInterval: 5000,
    flushHeaders: true,
    flushAfterWrite: false,
    serializer: JSON.stringify,
  };

  public static configure(config: Partial<ISseMiddlewareOptions>) {
    this.config = {
      ...this.config,
      ...config
    }
  }

  public async handle(req: IHttpRequest, res: ISseResponse): Promise<void> {
    const {keepAliveInterval, flushHeaders, flushAfterWrite, serializer} = SSEMiddleware.config;

    res.writeHead(200, {
      'Content-Type': 'text/event-stream',
      'Cache-Control': 'no-cache, no-transform',
      'Connection': 'keep-alive',
    });

    res.write(':ok\n\n');

    if (flushHeaders) {
      res.flushHeaders();
    }

    // Start heartbeats (if not disabled)
    if (keepAliveInterval !== false) {
      if (typeof keepAliveInterval !== 'number') {
        throw new Error('keepAliveInterval must be a number or === false');
      }

      startKeepAlives(keepAliveInterval);
    }

    res.pushData = (data: SseValue, id?: string) => write(message(null, data, id, serializer));
    res.pushEvent = (event: string, data: SseValue, id?: string) => write(message(event, data, id, serializer));
    res.pushComment = (text: string) => write(comment(text));

    function write(chunk: any) {
      res.write(chunk);
      if (flushAfterWrite) {
        res.flushHeaders();
      }
    }

    function startKeepAlives(interval: number) {
      // Regularly send keep-alive SSE comments, clear interval on socket close
      const keepAliveTimer = setInterval(() => write(': sse-keep-alive\n'), interval);

      // When the connection gets closed (close=client, finish=server), stop the keep-alive timer
      res.once('close', () => clearInterval(keepAliveTimer));
      res.once('finish', () => clearInterval(keepAliveTimer));
    }
  }

}
