import { IMiddleware, UnauthorizedError } from '@viatsyshyn/ts-rest';
import { injectable, inject } from 'inversify';
import { UserModel } from '../models';

@injectable()
export class AuthMiddleware implements IMiddleware {

  @inject(Symbol.for('CurrentUser'))
  private user: UserModel | null;

  public async handle(): Promise<void> {
    if (!this.user) {
      // throw new UnauthorizedError();
    }
  }
}
