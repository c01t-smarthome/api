import { injectable, inject } from 'inversify';

import {
  UnauthorizedError,
  RequestError,
} from '@viatsyshyn/ts-rest';
import { StatusCode } from '@viatsyshyn/ts-rest';

import { IErrorResult } from '../models';
import { IErrorMiddleware, IHttpRequest, IHttpResponse } from '@viatsyshyn/ts-rest';
import {ILOGGERFACTORY, ILoggerFactory} from '../services/logger';

@injectable()
export class ErrorHandlerMiddleware implements IErrorMiddleware {

  constructor(
    @inject(ILOGGERFACTORY) private readonly loggerFactory: ILoggerFactory
  ) { }

  public async handle(err: Error, req: IHttpRequest, res: IHttpResponse): Promise<void> {

    let errorResult: IErrorResult;
    if (err instanceof UnauthorizedError) {
      errorResult = {
        error: 'Not Authorized',
        code: err.statusCode
      }
    } else if (err instanceof RequestError && err.statusCode !== StatusCode.ServerError) {
      errorResult = {
        error: err.message,
        code: err.statusCode,
      }
    } else {
      const loggerName = req.url;
      const logger = this.loggerFactory(loggerName);
      logger.error(err);

      errorResult = {
        error: err.message,
        code: StatusCode.ServerError,
      }
    }

    return res
      .status(errorResult.code)
      .json(errorResult)
      .end();
  }
}
