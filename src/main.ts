import 'reflect-metadata';
import './middleware';
import './routes';

import { setUpRootContainer } from './ioc-container';
import { ApplicationServer } from './app-server';

const container = setUpRootContainer();
const SERVER = new ApplicationServer(container);

export default SERVER;
