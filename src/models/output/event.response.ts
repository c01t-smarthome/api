import {OpenApiDataType, OpenApiSchema, OpenApiSchemaProperty} from '@viatsyshyn/ts-openapi';

@OpenApiSchema()
export class EventResponse {

  @OpenApiSchemaProperty({
    format: 'uuid'
  })
  public device: string;

  @OpenApiSchemaProperty()
  public key: string;

  @OpenApiSchemaProperty({
    type: OpenApiDataType.object,
  })
  public value: any;

  @OpenApiSchemaProperty({
    format: 'date-time',
  })
  public lastUpdated: string;
}
