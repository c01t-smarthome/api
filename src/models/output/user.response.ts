import { OpenApiSchema, OpenApiSchemaProperty } from '@viatsyshyn/ts-openapi';

@OpenApiSchema({
  description: 'Bus User Response Model',
})
export class UserResponseModel {
  @OpenApiSchemaProperty({
    description: 'User UUID',
    format: 'uuid',
  })
  public uid: string;

  @OpenApiSchemaProperty({
    description: 'UserName'
  })
  public userName: string;

  @OpenApiSchemaProperty({
    description: 'User Email',
    nullable: true,
    format: 'email',
  })
  public email: string | null;

  @OpenApiSchemaProperty({
    description: 'Type'
  })
  public type: number;
}
