import {IWidget} from '../db';
import {OpenApiSchema, OpenApiSchemaProperty} from '@viatsyshyn/ts-openapi';
import {OpenApiDataType} from '@viatsyshyn/ts-openapi';

@OpenApiSchema({
  description: 'Device Response Model',
})
export class DeviceResponseModel {
  @OpenApiSchemaProperty({
    description: 'UUID',
    format: 'uuid',
  })
  public uid: string;

  @OpenApiSchemaProperty({
    description: 'Tag'
  })
  public tag: string;

  @OpenApiSchemaProperty({
    description: 'Name'
  })
  public name: string;

  @OpenApiSchemaProperty({
    description: 'description'
  })
  public description: string;

  @OpenApiSchemaProperty({
    description: 'firmware'
  })
  public firmware: string;

  @OpenApiSchemaProperty({
    description: 'Zone UUID',
    nullable: true,
    format: 'uuid',
  })
  public zone: string | null;

  @OpenApiSchemaProperty({
    description: 'Assets',
    type: OpenApiDataType.object,
  })
  public assets: {[key: string]: string};

  @OpenApiSchemaProperty({
    description: 'Icon Widget',
    type: OpenApiDataType.object,
  })
  public icon: IWidget;

  @OpenApiSchemaProperty({
    description: 'All Widgets',
    type: OpenApiDataType.array,
    items: OpenApiDataType.object,
  })
  public widgets: IWidget[];

  @OpenApiSchemaProperty({
    description: 'Attributes current values',
    type: OpenApiDataType.object,
  })
  public attributes: Record<string, any>;
}
