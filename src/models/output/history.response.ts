import {OpenApiDataType, OpenApiSchema, OpenApiSchemaProperty} from '@viatsyshyn/ts-openapi';
import {IAttribute} from '../../models';

export interface IHistoryRecord {
  timestamp: string;
  value: string;
}

@OpenApiSchema()
export class HistoryResponse {

  @OpenApiSchemaProperty()
  public attribute: string;

  @OpenApiSchemaProperty({
    type: OpenApiDataType.object,
  })
  public definition: IAttribute;

  @OpenApiSchemaProperty({
    type: OpenApiDataType.object,
  })
  public data: IHistoryRecord[]
}
