import {OpenApiDataType, OpenApiSchema, OpenApiSchemaProperty} from '@viatsyshyn/ts-openapi';
import {IAttribute, ISetting} from '../db';

@OpenApiSchema({
  description: 'Attribute response model',
})
export class AttributeResponse {

  @OpenApiSchemaProperty({
    format: 'uuid',
  })
  public uid: string;

  @OpenApiSchemaProperty()
  public key: string;

  @OpenApiSchemaProperty()
  public title: string;

  @OpenApiSchemaProperty({
    enum: ['in', 'out'],
  })
  public kind: 'in' | 'out';

  @OpenApiSchemaProperty({
    type: OpenApiDataType.object,
  })
  public definition: IAttribute;

  @OpenApiSchemaProperty({
    type: OpenApiDataType.object,
    nullable: true,
  })
  public setting: ISetting | null;

  @OpenApiSchemaProperty({
    description: 'Attribute current value, applies to kind: out',
    type: OpenApiDataType.object,
    nullable: true,
  })
  public value: any;
}
