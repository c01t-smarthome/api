import {OpenApiSchema, OpenApiSchemaProperty} from '@viatsyshyn/ts-openapi';
import {OpenApiDataType} from '@viatsyshyn/ts-openapi';

@OpenApiSchema({
  description: 'Version description',
})
export class VersionResponseModel {
  @OpenApiSchemaProperty({
    description: 'Id of version',
    type: OpenApiDataType.integer,
  })
  public id: number;

  @OpenApiSchemaProperty({
    description: '',
  })
  public name: string;

  @OpenApiSchemaProperty({
    description: 'Description of version',
  })
  public description: string;
}
