import {OpenApiDataFormat, OpenApiSchema, OpenApiSchemaProperty} from '@viatsyshyn/ts-openapi';
@OpenApiSchema({
  description: 'AuthenticationModel description',
})
export class AuthenticationResponseModel {
  @OpenApiSchemaProperty({
    description: 'Authenticated?',
  })
  public auth: boolean;

  @OpenApiSchemaProperty({
    description: 'Token',
    format: OpenApiDataFormat.byte,
    nullable: true,
  })
  public token: string | null;
}
