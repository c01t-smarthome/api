import {OpenApiDataType, OpenApiSchema, OpenApiSchemaProperty} from '@viatsyshyn/ts-openapi';

@OpenApiSchema({
  description: 'Error Response'
})
export class ErrorResponseModel {
  @OpenApiSchemaProperty({
    type: OpenApiDataType.integer
  })
  public code: number;

  @OpenApiSchemaProperty({
    description: 'Error message'
  })
  public error: string;
}
