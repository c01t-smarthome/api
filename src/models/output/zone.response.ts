import { OpenApiSchema, OpenApiSchemaProperty } from '@viatsyshyn/ts-openapi';

@OpenApiSchema({
  description: 'Zone Response Model',
})
export class ZoneResponseModel {
  @OpenApiSchemaProperty({
    description: 'UUID',
    format: 'uuid',
  })
  public uid: string;

  @OpenApiSchemaProperty({
    description: 'Tag'
  })
  public tag: string;

  @OpenApiSchemaProperty({
    description: 'Name'
  })
  public name: string;

  @OpenApiSchemaProperty({
    description: 'color',
    nullable: true,
  })
  public color: string | null;
}
