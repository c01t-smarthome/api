import {OpenApiDataType} from '@viatsyshyn/ts-openapi';
import {IOpenApiParameterArgs} from '@viatsyshyn/ts-openapi/dist/src/decorators/interfaces';

export const SORT: IOpenApiParameterArgs = {
  description: `Sort pattern`,
  schema: {
    type: OpenApiDataType.string,
  }
};

export const OFFSET: IOpenApiParameterArgs = {
  description: `Offset`,
  schema: {
    type: OpenApiDataType.integer,
    minimum: 0,
  }
};

export const LIMIT: IOpenApiParameterArgs = {
  description: `Limit`,
  schema: {
    type: OpenApiDataType.integer,
    minimum: 1,
    maximum: 100,
  }
};

export const COUNT_ONLY: IOpenApiParameterArgs = {
  description: `Get total count`,
  schema: {
    type: OpenApiDataType.boolean,
  },
};

export const UID: IOpenApiParameterArgs = {
  description: 'UUID',
  required: true,
  schema: {
    type: OpenApiDataType.string,
    format: 'uuid',
  }
};
