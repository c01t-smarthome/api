export interface IErrorResult {
  error: string;
  code: number;
}
