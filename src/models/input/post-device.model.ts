import { validate } from '../../services';
import {OpenApiSchema, OpenApiSchemaProperty} from '@viatsyshyn/ts-openapi';

@OpenApiSchema({
  description: 'Device Put description',
})
export class PostDeviceModel {

  @validate({
    required: false,
    type: 'string',
  })
  @OpenApiSchemaProperty({
    description: 'Device tag',
  })
  public tag: string;

  @validate({
    required: false,
    type: 'string',
  })
  @OpenApiSchemaProperty({
    description: 'Device manifest json',
  })
  public manifest: any;
}
