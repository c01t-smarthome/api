import { validate } from '../../services';
import {OpenApiSchema, OpenApiSchemaProperty} from '@viatsyshyn/ts-openapi';

@OpenApiSchema({
  description: 'Zone Put description',
})
export class PutZoneModel {

  @validate({
    required: false,
    type: 'string',
  })
  @OpenApiSchemaProperty({
    description: 'Zone tag',
    nullable: true,
  })
  public tag?: string;

  @validate({
    required: false,
    type: 'string',
  })
  @OpenApiSchemaProperty({
    description: 'Zone name',
    nullable: true,
  })
  public name?: string;

  @validate({
    required: false,
    type: 'string',
  })
  @OpenApiSchemaProperty({
    description: 'Zone color',
    nullable: true,
  })
  public color?: string;
}
