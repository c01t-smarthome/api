import { OpenApiSchema, OpenApiSchemaProperty } from '@viatsyshyn/ts-openapi';

@OpenApiSchema({
  description: 'Authentication description',
})
export class AuthenticateModel {
  @OpenApiSchemaProperty({
    description: 'Login',
    minLength: 3,
  })
  public username: string;

  @OpenApiSchemaProperty({
    description: 'Password',
    minLength: 3,
  })
  public password: string;
}
