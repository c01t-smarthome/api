import {DeviceModel} from '../db';

export class GetDeviceModel implements Omit<DeviceModel, 'id'|'uid'|'icons'|'widgets'|'forgotten'|'created'|'modified'> {
  public name: string;
  public tag: string;
  public description: string;
  public firmware: string;
  public zone: number | null;
}
