import {validate} from '../../services';
import {OpenApiDataType, OpenApiSchema, OpenApiSchemaProperty} from '@viatsyshyn/ts-openapi';
import {ISetting} from '../db';

@OpenApiSchema({
  description: 'Attribute Put description',
})
export class PutAttributeModel {

  @validate({
    required: false,
    type: 'string',
  })
  @OpenApiSchemaProperty({
    description: 'Device name',
    type: OpenApiDataType.object,
    nullable: true,
  })
  public setting?: ISetting;

  @OpenApiSchemaProperty({
    description: 'Device zone',
    type: OpenApiDataType.object,
    nullable: true,
  })
  public value?: any;
}
