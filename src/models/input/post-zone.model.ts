import { OpenApiSchema, OpenApiSchemaProperty } from '@viatsyshyn/ts-openapi';
import { validate } from '../../services';

@OpenApiSchema({
  description: 'Zone Post description',
})
export class PostZoneModel {

  @validate({
    required: true,
    type: 'string',
  })
  @OpenApiSchemaProperty({
    description: 'Zone tag',
  })
  public tag: string;

  @validate({
    required: true,
    type: 'string',
  })
  @OpenApiSchemaProperty({
    description: 'Zone name',
  })
  public name: string;

  @validate({
    required: false,
    type: 'string',
  })
  @OpenApiSchemaProperty({
    description: 'Zone color',
    nullable: true,
  })
  public color?: string;
}
