import {validate} from '../../services';
import {OpenApiDataFormat, OpenApiSchema, OpenApiSchemaProperty} from '@viatsyshyn/ts-openapi';
import {OpenApiDataType} from '@viatsyshyn/ts-openapi';

@OpenApiSchema({
  description: 'Put User Model',
})
export class PutUserModel {

  @OpenApiSchemaProperty({
    description: 'Email',
    format: 'email',
    nullable: true,
  })
  @validate({
    required: false,
    type: 'email',
  })
  public email?: string;

  @OpenApiSchemaProperty({
    description: 'Type',
    nullable: true,
    type: OpenApiDataType.integer,
    minimum: 0,
  })
  @validate({
    required: false,
    type: 'positiveInt',
  })
  public type?: number;

  @OpenApiSchemaProperty({
    description: 'New password',
    nullable: true,
    format: OpenApiDataFormat.password,
  })
  @validate({
    required: false,
  })
  public password?: string;

  @OpenApiSchemaProperty({
    description: 'Current password, required for /me',
    nullable: true,
    format: OpenApiDataFormat.password,
  })
  @validate({
    required: false,
  })
  public confirmation?: string;
}
