import { validate } from '../../services';
import {OpenApiSchema, OpenApiSchemaProperty} from '@viatsyshyn/ts-openapi';

@OpenApiSchema({
  description: 'Device Put description',
})
export class PutDeviceModel {

  // @validate({
  //   required: false,
  //   type: 'string',
  // })
  // @ApiModelProperty({
  //   description: 'Device tag',
  //   required: false
  // })
  // public tag?: string;

  @validate({
    required: false,
    type: 'string',
  })
  @OpenApiSchemaProperty({
    description: 'Device name',
    nullable: true,
  })
  public name?: string;

  // @validate({
  //   required: false,
  //   type: 'string',
  // })
  // @ApiModelProperty({
  //   description: 'Device description',
  //   required: false
  // })
  // public description?: string;

  @validate({
    required: false,
    type: 'string',
  })
  @OpenApiSchemaProperty({
    description: 'Device zone',
    format: 'uuid',
    nullable: true,
  })
  public zone?: string | null;
}
