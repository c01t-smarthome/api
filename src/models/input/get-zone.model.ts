import {ZoneModel} from '../db';

export class GetZoneModel implements Omit<ZoneModel, 'id'|'uid'|'created'|'modified'> {
  public name: string;
  public tag: string;
  public color?: string;
}
