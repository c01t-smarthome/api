import {OpenApiDataFormat, OpenApiSchema, OpenApiSchemaProperty} from '@viatsyshyn/ts-openapi';
import { validate } from '../../services';

@OpenApiSchema({
  description: 'User FCM Post description',
})
export class PostUserFcmModel {

  @validate({
    required: true,
    type: 'string',
  })
  @OpenApiSchemaProperty({
    description: 'Registration Token',
    format: OpenApiDataFormat.byte,
  })
  public token: string;
}
