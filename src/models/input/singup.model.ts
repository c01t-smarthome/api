import {OpenApiDataFormat, OpenApiSchema, OpenApiSchemaProperty} from '@viatsyshyn/ts-openapi';
import { validate } from '../../services';

@OpenApiSchema({
  description: 'SignUp description',
})
export class SignUpModel {

  @validate({
    required: true,
    type: 'string',
  })
  @OpenApiSchemaProperty({
    description: 'Admin Email',
    format: 'email',
  })
  public email: string;

  @validate({
    required: true,
    type: 'string',
  })
  @OpenApiSchemaProperty({
    description: 'QR Code',
    format: OpenApiDataFormat.byte,
  })
  public token: string;
}
