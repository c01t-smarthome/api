import {AttributeModel} from '../db';

export class GetAttributeModel implements Omit<AttributeModel, 'id'|'uid'|'device'|'definition'|'setting'|'created'|'modified'> {
  public key: string;
  public title: string;
  public kind: 'in' | 'out';
}
